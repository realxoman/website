export function toPersianDigits(value) {
  let i = 0
  let num = value.toString()
  let len = num.length
  let res = ""
  let persianNumbers = ["۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"]
  for (; i < len; i++)
    if (persianNumbers[num.charAt(i)]) res += persianNumbers[num.charAt(i)]
    else res += num.charAt(i)

  return res
}
