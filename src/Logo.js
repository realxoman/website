import React from "react"

const SvgComponent = props => (
  <svg height={256} width={256} {...props}>
    <defs>
      <linearGradient id="prefix__c">
        <stop offset={0} stopColor="#f2ffff" />
        <stop offset={1} stopColor="#fff" stopOpacity={0.591} />
      </linearGradient>
      <linearGradient
        gradientTransform="matrix(1.16754 0 0 1.16754 -1295.513 -553.049)"
        y2={585.562}
        x2={1270.492}
        y1={585.562}
        x1={1150.461}
        gradientUnits="userSpaceOnUse"
        id="prefix__f"
        xlinkHref="#prefix__a"
      />
      <linearGradient
        y2={585.562}
        x2={1270.492}
        y1={585.562}
        x1={1150.461}
        gradientUnits="userSpaceOnUse"
        id="prefix__a"
      >
        <stop offset={0} stopColor="#fff" stopOpacity={0.658} />
        <stop offset={1} stopColor="#fff" />
      </linearGradient>
      <linearGradient
        y2={621.236}
        x2={654.184}
        y1={621.236}
        x1={458.278}
        gradientTransform="matrix(-1 0 0 1 1789.387 0)"
        gradientUnits="userSpaceOnUse"
        id="prefix__e"
        xlinkHref="#prefix__b"
      />
      <linearGradient
        gradientTransform="matrix(-1 0 0 1 1789.387 0)"
        y2={621.236}
        x2={654.184}
        y1={621.236}
        x1={458.278}
        gradientUnits="userSpaceOnUse"
        id="prefix__b"
      >
        <stop offset={0} stopColor="#fff" stopOpacity={0.528} />
        <stop offset={1} stopColor="#fff" />
      </linearGradient>
      <linearGradient
        gradientUnits="userSpaceOnUse"
        y2={565.648}
        x2={1312.354}
        y1={565.648}
        x1={1112.223}
        id="prefix__d"
        xlinkHref="#prefix__c"
      />
    </defs>
    <g
      transform="matrix(1.16754 0 0 1.16754 -1295.513 -553.049)"
      strokeWidth={0.856}
      fill="url(#prefix__d)"
    >
      <path d="M1164.118 538.513s7.496 9.435 5.707-.229c-1.791-9.665-16.127-28.551-23.35-23.98-7.227 4.57-3.574 8.335.963 10.764 4.541 2.428 13.486 8.582 16.68 13.445zM1202.237 523.854s-1.49 12.77 4.471 4.14c5.955-8.629 9.438-33.689.545-35.706-8.895-2.019-8.99 3.573-7.408 8.831 1.583 5.257 3.665 16.658 2.392 22.735zM1244.338 528.282s-9.543 11.699 1.805 6.922c11.348-4.782 30.627-27.39 23.094-35.007-7.531-7.614-11.16-2.137-12.91 4.068-1.756 6.207-6.895 18.804-11.989 24.017zM1274.813 558.616s-14.912 4.14-2.586 6.859c12.326 2.715 41.84-4.787 40.049-15.621-1.789-10.833-8.07-8.397-13.213-4.241-5.143 4.154-16.9 11.656-24.25 13.003zM1140.122 567.282s9.912 3.295 4.027-2.868c-5.883-6.168-25.25-13.072-28.322-6.325-3.074 6.745 1.359 7.741 5.809 7.349 4.443-.392 13.859-.167 18.486 1.844zM1133.073 599.112s8.361-.392 2.186-3.31c-6.18-2.914-22.656-2.503-23.012 3.43-.355 5.931 3.252 5.398 6.463 3.826 3.21-1.576 10.318-4.12 14.363-3.946zM1140.409 627.192s4.957-4.282-.254-3.095c-5.207 1.188-15.152 9.398-12.506 13.225 2.639 3.823 4.607 1.756 5.824-.764 1.219-2.522 4.361-7.519 6.936-9.366z" />
    </g>
    <path
      d="M78.973 131.511c8.831-21.095 33.296-30.954 54.644-22.014a42.17 42.17 0 0113.008 8.545 45.11 45.11 0 00-16.133-11.428c-22.815-9.551-48.96.986-58.398 23.53-5.049 12.06-5.337 27.023-.57 38.963a260.64 260.64 0 017.63-5.5c-4.245-9.92-4.65-21.421-.181-32.096z"
      fill="none"
    />
    <path
      d="M117.828 114.645c-19.186 0-34.792 15.606-34.792 34.791 0 3.645.574 7.156 1.623 10.46 17.653-11.56 38.918-22.233 63.034-28.32-.011 0-.017 0-.024-.002-6.089-10.132-17.185-16.93-29.841-16.93zM186.914 137.485c.456 2.66.782 5.365.93 8.117-7.486.567-11.57-.75-11.57-.75-3.3-3.71-7.037-6.406-10.825-8.367a82.481 82.481 0 00-7.569 1.153c-33.373 6.682-61.547 22.878-82.508 38.75.778 1.152 1.596 2.272 2.502 3.31-4.223 7.497-6.052 11.97-9.235 19.701a70.73 70.73 0 01-8.684-10.369c-8.723 7.728-15.389 14.663-19.693 19.442h213.553c-6.518-17.995-26.503-62.507-66.901-70.987z"
      fill="none"
    />
    <path
      d="M1269.696 591.441a57.18 57.18 0 00-11.784-1.211c-2.16 0-4.366.121-6.6.354-2.14.225-4.306.552-6.483.988-28.584 5.723-52.715 19.595-70.668 33.189a222.225 222.225 0 00-13.205 10.828c-7.471 6.619-13.18 12.559-16.867 16.652h182.908c-5.583-15.413-22.7-53.537-57.301-60.8z"
      fill="url(#prefix__e)"
      transform="matrix(1.16754 0 0 1.16754 -1295.513 -553.049)"
      strokeWidth={0.856}
    />
    <path
      d="M72.093 130.145c9.438-22.547 35.583-33.082 58.398-23.53a45.11 45.11 0 0116.133 11.427 42.148 42.148 0 00-13.008-8.545c-21.348-8.939-45.812.919-54.644 22.014-4.47 10.676-4.063 22.175.18 32.096a253.623 253.623 0 015.506-3.712 34.54 34.54 0 01-1.623-10.459c0-19.185 15.606-34.791 34.791-34.791 12.657 0 23.753 6.796 29.842 16.929l.024.002c2.823-.712 5.681-1.37 8.583-1.951 5.724-1.146 11.404-1.727 16.879-1.727 4.06 0 7.957.318 11.701.9-8.811-28.657-35.487-49.487-67.029-49.487-38.73 0-70.125 31.396-70.125 70.125A69.733 69.733 0 0055.7 181.93a268.314 268.314 0 0115.824-12.824c-4.767-11.942-4.479-26.903.57-38.962z"
      fill="url(#prefix__f)"
    />
    <text
      y={222}
      x={218}
      style={{
        lineHeight: 1000,
      }}
      fontWeight={400}
      fontSize={40}
      fontFamily="sans-serif"
      letterSpacing={0}
      wordSpacing={0}
    />
  </svg>
)

export default SvgComponent
