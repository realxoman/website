import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"
import { Media } from "reactstrap"

import "./about.scss"

const Cake = props => {
  const data = useStaticQuery(graphql`
    query {
      cake: file(relativePath: { eq: "cake.jpg" }) {
        childImageSharp {
          fluid(maxWidth: 500, grayscale: true) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `)

  return <Img {...props} fluid={data.cake.childImageSharp.fluid} />
}

function about() {
  return (
    <Media className="about">
      <Media right>
        <div className="about__image">
          <Cake className="img-fluid" />
        </div>
      </Media>
      <Media body className="about__media-body mr-3">
        <p>
          روز آزادی نرم افزار یا به اختصار (SFD) یک جشن سالیانه جهانی برای نرم
          افزار‌های آزاد است و از سال ۱۳۸۳ در ایران برگزار می‌شود. سال گذشته
          مدافعان آزادی نرم افزار این جشن را در ۷۱ نقطه مختلف در سراسر جهان
          برگزار کردند.
        </p>
        <p>
          اولین جشن آزادی نرم افزار در ۷ شهریور ۱۳۸۳ در ۶۴ مکان مختلف (در ۳۰
          کشور جهان) برگزار شد که تهران، اصفهان، مشهد، گیلان و کرمان میزبانان
          این جشن در کشور ایران بودند.
        </p>
        <p>
          امسال باغ کتاب میزبان برگزاری این جشن در تهران است که در ۸ شهریور (چند
          روز زود تر از رویداد جهانی) برگزار می‌شود.
        </p>
      </Media>
    </Media>
  )
}

export default about
