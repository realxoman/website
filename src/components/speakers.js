import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"

import "./speakers.scss"

const useSpeakersDate = () => {
  const data = useStaticQuery(graphql`
    query {
      allSpeakersJson(sort: { fields: [name], order: ASC }) {
        nodes {
          name
          twitter
          github
          gitlab
          jobTitle
          image {
            childImageSharp {
              fluid(maxWidth: 170, maxHeight: 170, grayscale: true) {
                ...GatsbyImageSharpFluid
              }
            }
          }
        }
      }
    }
  `)

  return data.allSpeakersJson.nodes
}

function speakers() {
  const data = useSpeakersDate()
  return (
    <>
      <h2 className="pb-2">سخنرانان</h2>
      <ul className="speakers__list d-flex flex-cloumn flex-wrap justify-content-start">
        {data.map(person => (
          <Speaker key={person.name} {...person} />
        ))}
      </ul>
    </>
  )
}

function Speaker({ image, name, twitter, github, gitlab, jobTitle }) {
  return (
    <li>
      <Img fluid={image.childImageSharp.fluid} className="speaker__image" />
      <div className="speaker__text">
        <div className="speaker__name">
          <p className="d-inline">{name}</p>
          {twitter && (
            <a href={`https://twitter.com/${twitter}`} target="_blank">
              <Twitter className="mr-2 mt-1" />
            </a>
          )}
          {github && (
            <a href={`https://github.com/${github}`} target="_blank">
              <Github className="mr-2 mt-1 speaker__github" />
            </a>
          )}
          {gitlab && (
            <a href={`https://gitlab.com/${gitlab}`} target="_blank">
              <Gitlab className="mr-2 mt-1" />
            </a>
          )}
        </div>
        <p className="text-muted mt-2">{jobTitle}</p>
      </div>
    </li>
  )
}

function Twitter(props) {
  return (
    <svg width="15" height="15" fill="none" {...props}>
      <g opacity="0.5">
        <path
          d="M7.499 14.982a7.487 7.487 0 1 0 0-14.975 7.487 7.487 0 0 0 0 14.975z"
          fill="#9BABFF"
        ></path>
        <path
          d="M12.994 4.452a3.79 3.79 0 0 1-1.118.317c.402-.25.71-.644.856-1.114-.376.23-.792.398-1.237.489a1.915 1.915 0 0 0-1.42-.636c-1.076 0-1.948.901-1.948 2.013 0 .157.018.31.05.458-1.618-.084-3.052-.885-4.013-2.102a2.054 2.054 0 0 0-.263 1.012c0 .698.343 1.314.866 1.674-.32-.01-.62-.1-.882-.251v.025c0 .975.67 1.788 1.562 1.973a1.878 1.878 0 0 1-.88.035c.248.8.967 1.381 1.819 1.398a3.827 3.827 0 0 1-2.883.833 5.382 5.382 0 0 0 2.985.904c3.58 0 5.54-3.066 5.54-5.725 0-.087-.002-.174-.007-.26.382-.286.712-.64.973-1.043z"
          fill="#000"
        ></path>
      </g>
    </svg>
  )
}

function Github(props) {
  return (
    <svg viewBox="0 0 15 15" fill="none" width="1em" height="1em" {...props}>
      <path
        opacity="0.5"
        fill-rule="evenodd"
        clip-rule="evenodd"
        d="M7.5 0C3.357 0 0 3.443 0 7.69c0 3.397 2.149 6.28 5.13 7.296.374.071.511-.166.511-.37 0-.182-.006-.666-.01-1.308-2.086.465-2.526-1.03-2.526-1.03-.342-.89-.833-1.126-.833-1.126-.681-.477.051-.467.051-.467.753.054 1.149.792 1.149.792.669 1.175 1.756.836 2.183.64.068-.497.262-.836.476-1.029-1.665-.193-3.416-.853-3.416-3.8 0-.84.292-1.526.772-2.064-.078-.194-.335-.976.073-2.035 0 0 .63-.206 2.063.789a7.018 7.018 0 0 1 1.877-.26 7.025 7.025 0 0 1 1.878.26c1.432-.995 2.06-.789 2.06-.789.41 1.059.152 1.84.075 2.035.48.538.771 1.224.771 2.064 0 2.954-1.754 3.604-3.424 3.794.269.238.509.707.509 1.425 0 1.027-.01 1.857-.01 2.109 0 .206.136.445.516.37C12.853 13.966 15 11.086 15 7.69 15 3.443 11.642 0 7.5 0z"
        fill="#9BABFF"
      ></path>
    </svg>
  )
}

function Gitlab(props) {
  return (
    <svg
      width={45}
      height={45}
      viewBox="0 0 45 45"
      style={{ width: "18px", height: "18px", opacity: "0.5" }}
      {...props}
    >
      <path d="M24 43l-8-23h16zm0 0" fill="#9BABFF" />
      <path d="M24 43l18-23H32zm0 0" fill="#9BABFF" />
      <path d="M37 5l5 15H32zm0 0" fill="#9BABFF" />
      <path d="M24 43l18-23 3 8zm0 0" fill="#9BABFF" />
      <path d="M24 43L6 20h10zm0 0" fill="#9BABFF" />
      <path d="M11 5L6 20h10zm0 0" fill="#9BABFF" />
      <path d="M24 43L6 20l-3 8zm0 0" fill="#9BABFF" />
    </svg>
  )
}

export default speakers
