import React from "react"
import Countdown from "../components/countdown"

import { useStaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"

import "./header.scss"

// 8 Shahrivar 1398, @ 14:00
const date = new Date("August 30, 2019 14:00:00 +0330")

const Logo = props => {
  const data = useStaticQuery(graphql`
    query {
      softwarefreedomday: file(relativePath: { eq: "softwarefreedomday.png" }) {
        childImageSharp {
          fluid(maxWidth: 300) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `)

  return (
    <Img {...props} fluid={data.softwarefreedomday.childImageSharp.fluid} />
  )
}

const Header = ({ siteTitle }) => (
  <header className="header">
    <h1 className="header__title">{siteTitle}</h1>
    <div className="header__leftside">
      <Logo className="header__logo" />
      <div>
        <p className="header__date">۸ شهریور ۱۳۹۸</p>
        <p className="header__hour">ساعت ۱۴ تا ۲۱ - باغ کتاب تهران</p>
        <Countdown date={date} />
        <a href="#map">
          <button className="header__letsgo">بزن بریم!</button>
        </a>
      </div>
    </div>
  </header>
)

export default Header
